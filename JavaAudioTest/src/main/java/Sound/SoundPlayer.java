/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sound;

import Audio.Audio;
import Data.SoundModel;
import Audio.Player;
import Audio.TrackFinishedListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.Mixer;

/**
 *
 * @author pi
 */
/*public class SoundPlayer implements Comparable<SoundPlayer> {
    Mixer.Info mixerInfo;
    String name;
    String hub;
    Integer port;
    
    Player audioPlayer;
    private Map<String, Audio> audios = new HashMap<String, Audio>();
    
    public SoundPlayer(String name, Mixer.Info mixerInfo) {
        this.mixerInfo = mixerInfo;
        
        this.name = name;
        //this.name = name.substring(0, 6);
        //this.hub = this.name.substring(4, 5);
        //String portString = this.name.substring(5, 6);
        //port = Integer.parseInt(portString);
        
        audioPlayer = new Player(mixerInfo);
    }
    
    public void PlaySound(SoundModel sound, String soundId, float volume, float balance) 
    {
        try {
            if(!audioPlayer.isTrackRegistered(sound.name))
            {
                audioPlayer.registerTrackWithoutDecoding(sound.getPath().toString(), sound.name);
            }
            
            Audio audio = audioPlayer.createAudio(sound.name);
            audio.addTrackFinishedListener(new TrackFinishedListener() {
                @Override
                public void trackFinished(Audio src) throws IOException {
                    removeAudio(soundId);
                }
            });
            addAudio(audio, soundId);
            audio.setVolume(volume);
            audio.setBalance(balance);
            audio.play();
        } catch (Exception ex) {
            Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public int compareTo(SoundPlayer other) {
        if (hub == null ? other.hub == null : hub.equals(other.hub)) {
            return port.compareTo(other.port);
        } else {
            return hub.compareTo(other.hub);
        }
    }
    
    synchronized void addAudio(Audio audio, String soundId)
    {
        audios.put(soundId, audio);
    }
    
    synchronized void removeAudio(String soundId)
    {
        audios.remove(soundId);
    }
    
    synchronized public AudioState getState(String soundId)
    {
        Audio audio = audios.get(soundId);
        if (audio == null) { return null; }
        
        float balance = audio.getBalance();
        float volume = audio.getVolume();
        
        AudioState state = new AudioState();
        state.volume = volume;
        state.balance = balance;
        
        return state;
    }
    
    synchronized public boolean setState(String soundId, AudioState state)
    {
        Audio audio = audios.get(soundId);
        if (audio == null) { return false; }
        
        if(state.shouldChangeVolume)
        {
            audio.setVolume(state.volume);
        }
        if(state.shouldChangeBalance)
        {
            audio.setBalance(state.balance);
        }
        if(state.shouldStop)
        {
            audio.stop();
        }
        return true;
    }
}*/
