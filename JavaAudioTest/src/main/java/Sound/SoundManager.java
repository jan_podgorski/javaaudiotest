/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sound;

import Audio.Converter;
import Audio.Player;
import Data.FileManager;
import Data.SoundModel;
import Util.Threads;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author pi
 */
/*public class SoundManager {

    private final ArrayList<SoundPlayer> players = new ArrayList<>();
    private final Map<String, SoundModel> sounds = new HashMap<String, SoundModel>();

    public SoundManager() {
    }
    
    public void ProcessSoundCommand(Command_Sound soundCommand)
    {
        SoundPlayer player = PlayerForSoundCommand(soundCommand);
        SoundModel sound = sounds.get(soundCommand.file);
        if(player != null && sound != null)
        {
            player.PlaySound(sound, soundCommand.commandId, soundCommand.volume, soundCommand.balance);
        }
    }
    
    public void ProcessSoundParameterCommand(Command_SoundParameter parameterCommand)
    {
        SoundPlayer player = PlayerForSoundParameterCommand(parameterCommand);
        if(player == null) { return; }
        
        if (parameterCommand.duration > 0.0f) {
            changeParameterOverTime(player, parameterCommand);
        } else {
            changeParameterImmediatelly(player, parameterCommand);
        }
    }
    
    private void changeParameterImmediatelly(SoundPlayer player, Command_SoundParameter command)
    {
        AudioState state = new AudioState();
        state.shouldChangeVolume = command.changeVolume;
        state.shouldChangeBalance = command.changeBalance;
        state.shouldStop = command.stop;
        state.volume = command.targetVolume;
        state.balance = command.targetBalance;
        player.setState(command.expectedCommandId, state);
    }
    
    private void changeParameterOverTime(SoundPlayer player, Command_SoundParameter command)
    {
        AudioState startingState = new AudioState();
        int steps = command.steps;
        float divisions = (float)(command.steps + 1);
        
        float durationNanos = command.duration * 1000000000.0f;
        float durationStepTotalNanos = durationNanos / divisions;
        long durationStepMillis = (long)(durationStepTotalNanos / 1000000.0f);
        
        float startingVolume = command.startingVolume;
        float targetVolume = command.targetVolume;
        float volumeStep = (targetVolume - startingVolume) / divisions;
        
        float startingBalance = command.startingBalance + 1.0f;
        float targetBalance = command.targetBalance + 1.0f;
        float balanceStep = (targetBalance - startingBalance) / divisions;
        System.out.format("Starting balance: %f \n", startingBalance - 1.0f);
        System.out.format("Targety balance: %f \n", targetBalance - 1.0f);
        
        AudioState targetState = new AudioState();
        targetState.shouldChangeVolume = command.changeVolume;
        targetState.shouldChangeBalance = command.changeBalance;
        
        Threads.async(() -> {
            for (int index = 0; index <= steps; index++) {
                targetState.volume = startingVolume + (float) index * volumeStep;
                targetState.balance = (startingBalance + (float) index * balanceStep) - 1.0f;
                
                System.out.format("Setting balance: %f \n", targetState.balance);

                player.setState(command.expectedCommandId, targetState);

                try {
                    Thread.sleep(durationStepMillis);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SoundManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void SetupWithSounds(JSONArray soundsJsonArray) {
        for (int index = 0; index < soundsJsonArray.length(); index++) {
            JSONObject object = soundsJsonArray.optJSONObject(index);
            SoundModel model = new SoundModel(object);
            model.checkCachedFiles();
            sounds.put(model.name, model);
        }
    }
    
    public List<SoundModel> requiredFiles()
    {
        List<SoundModel> files = new ArrayList<>();
        for(Map.Entry<String, SoundModel> entry : sounds.entrySet())
        {
            SoundModel model = entry.getValue();
            files.add(model);
        }
        return files;
    }
    
    public void DecodeFiles()
    {
        for(Map.Entry<String, SoundModel> entry : sounds.entrySet())
        {
            SoundModel model = entry.getValue();
            if(model.requiresDecoding())
            {
                String sourcePath = model.getIntermediatePath().toString();
                String targetPath = model.prepareTargetFilePath();
                String tempDecodedFilePath = Defines.Defines.tempDecodedFilePath;
                File file = new File(sourcePath);
                try {
                    Converter.toFile(Files.readAllBytes(file.toPath()), Player.TARGET_FORMAT, tempDecodedFilePath);
                    FileManager.MoveFile(tempDecodedFilePath, targetPath);
                } catch (IOException ex) {
                    Logger.getLogger(SoundManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedAudioFileException ex) {
                    Logger.getLogger(SoundManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(SoundManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void SetupAdapters() {
        Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
        for (Mixer.Info info : mixerInfo) {
            String name = info.getName();
            String description = info.getDescription();
            String fullInfo = String.format("Name [%s] \nDescription [%s]\n\n", info.getName(), info.getDescription());
            System.out.print(fullInfo);

            //if (name.startsWith("EXT_") && name.length() >= 6) {
                SoundPlayer player = new SoundPlayer(name, info);
                players.add(player);
            //}
        }
        Collections.sort(players);
    }

    public int PlayerCount() {
        return players.size();
    }

    public String NameForPlayer(int index) {
        if (index < players.size()) {
            SoundPlayer player = players.get(index);
            return player.name;
        }
        return null;
    }

    public String HubForPlayer(int index) {
        if (index < players.size()) {
            SoundPlayer player = players.get(index);
            return player.hub;
        }
        return null;
    }

    public Integer PortForPlayer(int index) {
        if (index < players.size()) {
            SoundPlayer player = players.get(index);
            return player.port;
        }
        return null;
    }
    
    private SoundPlayer PlayerForSoundCommand(Command_Sound command)
    {
        for(SoundPlayer player : players)
        {
            if(Objects.equals(player.name, command.name)
                    && Objects.equals(player.hub, command.hub)
                    && Objects.equals(player.port, command.port)) {
                return player;
            }
        }
        return null;
    }
    
    private SoundPlayer PlayerForSoundParameterCommand(Command_SoundParameter command)
    {
        for(SoundPlayer player : players)
        {
            if(Objects.equals(player.name, command.name)
                    && Objects.equals(player.hub, command.hub)
                    && Objects.equals(player.port, command.port)) {
                return player;
            }
        }
        return null;
    }
}*/
