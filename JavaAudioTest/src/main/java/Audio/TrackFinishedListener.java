package Audio;

import java.io.IOException;

public interface TrackFinishedListener {
    void trackFinished(Audio src) throws IOException;
}
