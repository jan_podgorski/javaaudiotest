package Audio;

import javax.sound.sampled.*;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Player implements Closeable, AutoCloseable {
    private boolean closeCalled = false;
    private final ExecutorService workers = Executors.newCachedThreadPool();
    private final Map<String, byte[]> tracks = new HashMap<>();
    private final Mixer.Info info;

    private static final AtomicInteger NEXT_IDX = new AtomicInteger(0);
    public static final AudioFormat TARGET_FORMAT = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0f, 16, 2, 4, 44100.0f, false);
    private static final int CHUNK_SIZE = 32768;

    public Player(final Mixer.Info info){
        if(info == null){
            throw new IllegalArgumentException("info cannot be null!");
        }
        this.info = info;
    }

    public void registerTrack(final String path, final String label) throws IOException, UnsupportedAudioFileException {
        registerTrack(new File(path), label);
    }
    
    public void registerTrackWithoutDecoding(final String path, final String label) throws IOException, UnsupportedAudioFileException {
        registerTrackWithoutDecoding(new File(path), label);
    }

    public synchronized void registerTrack(final File file, final String label) throws IOException, UnsupportedAudioFileException {
        if(file == null || label == null){
            throw new IllegalArgumentException("file nor label cannot be null");
        }
        if(tracks.containsKey(label)){
            throw new IllegalArgumentException("track for label '" + label + "' already registered");
        }
        if(closeCalled){
            throw new IllegalStateException("close already called");
        }

        byte[] trackBytes = Converter.toBytes(Files.readAllBytes(file.toPath()), TARGET_FORMAT);
        tracks.put(label, trackBytes);
    }
    
    public synchronized void registerTrackWithoutDecoding(final File file, final String label) throws IOException, UnsupportedAudioFileException {
        if(file == null || label == null){
            throw new IllegalArgumentException("file nor label cannot be null");
        }
        if(tracks.containsKey(label)){
            throw new IllegalArgumentException("track for label '" + label + "' already registered");
        }
        if(closeCalled){
            throw new IllegalStateException("close already called");
        }

        byte[] trackBytes = Files.readAllBytes(file.toPath());
        tracks.put(label, trackBytes);
    }

    public synchronized void unregisterTrack(final String label){
        if(label == null){
            throw new IllegalArgumentException("label cannot be null");
        }
        if(!tracks.containsKey(label)){
            throw new IllegalArgumentException("track for label '" + label + "' is not registered");
        }
        if(closeCalled){
            throw new IllegalStateException("close already called");
        }

        tracks.remove(label);
    }
    
    public synchronized boolean isTrackRegistered(final String label)
    {
        if(label == null){
            throw new IllegalArgumentException("label cannot be null");
        }
        return tracks.containsKey(label);
    }

    public synchronized Audio createAudio(final String label) throws LineUnavailableException {
        if(label == null){
            throw new IllegalArgumentException("label cannot be null");
        }
        if(!tracks.containsKey(label)){
            throw new IllegalArgumentException("track for label '" + label + "' is not registered");
        }

        return new AudioImpl(tracks.get(label));
    }

    @Override
    public synchronized void close() {
        closeCalled = true;
        tracks.clear();
        workers.shutdown();
    }

    private class AudioImpl implements Audio{
        private final int idx = NEXT_IDX.getAndIncrement();
        private final byte[] trackBytes;
        private SourceDataLine sdl = null;
        private boolean playing = false;
        private boolean paused = false;
        private final List<TrackFinishedListener> listeners = new ArrayList<>();
        private float volume = 50.0f;
        private float balance = 0.0f;
        private final ReentrantLock lck = new ReentrantLock();
        private final ChunkReader chunkReader;
        public AudioImpl(final byte[] trackBytes) throws LineUnavailableException {
            assert(trackBytes != null);
            this.trackBytes = trackBytes;
            chunkReader = new ChunkReader(trackBytes, CHUNK_SIZE);
            final DataLine.Info info = new DataLine.Info(SourceDataLine.class, TARGET_FORMAT);
            sdl = (SourceDataLine) (AudioSystem.getLine(info));
            sdl.open(TARGET_FORMAT, CHUNK_SIZE);
        }

        @Override
        public float getVolume() {
            return volume;
        }

        @Override
        public void setVolume(float volume) {
            if(volume < 0.0f || volume > 100.0f){
                throw new IllegalArgumentException("volume must be in 0.0 .. 100.0");
            }
            ((FloatControl)(sdl.getControl(FloatControl.Type.MASTER_GAIN))).setValue((float)(20.0f * Math.log10(volume / 100)));
            this.volume = volume;
        }

        @Override
        public float getBalance() {
            return balance;
        }

        @Override
        public void setBalance(float balance) {
            if(balance < -1.0f || balance > 1.0f){
                throw new IllegalArgumentException("balance must be in -1.0 .. 1.0");
            }
            ((FloatControl)(sdl.getControl(FloatControl.Type.BALANCE))).setValue(balance);
            this.balance = balance;
        }

        @Override
        public boolean isPlaying() {
            return playing;
        }

        @Override
        public boolean isPaused() {
            return paused;
        }

        @Override
        public void play() {
            try{
                lck.lock();
                if(playing){
                    return;
                }

                sdl.start();
                playing = true;
                paused = false;

                workers.submit(() -> {
                    while (playing && !chunkReader.isFinished()) {
                        Range r = chunkReader.get();
                        sdl.write(trackBytes, r.getFrom(), r.getTo() - r.getFrom());
                        chunkReader.nextChunk();
                    }
                    if(!paused) {
                        sdl.drain();
                        stop();
                        for(final TrackFinishedListener l: listeners){
                            try {
                                l.trackFinished(this);
                            } catch (IOException e) {
                                System.err.println("Freaking Java, swear to God...");
                            }
                        }
                    }
                });
            }finally{
                lck.unlock();
            }
        }

        @Override
        public void pause() {
            try{
                lck.lock();
                sdl.stop();
                playing = false;
                paused = true;
            }finally{
                lck.unlock();
            }
        }

        @Override
        public void stop() {
            try{
                lck.lock();
                pause();
                paused = false;
                sdl.flush();
                chunkReader.reset();
            }finally{
                lck.unlock();
            }
        }

        @Override
        public void addTrackFinishedListener(TrackFinishedListener l) {
            if(l == null){
                throw new IllegalArgumentException("Listener cannot be null");
            }
            listeners.add(l);
        }

        @Override
        public void close() {
            stop();
            sdl.close();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof AudioImpl)) return false;
            AudioImpl audio = (AudioImpl) o;
            return idx == audio.idx;
        }

        @Override
        public int hashCode() {
            return Objects.hash(idx);
        }
    }
}
