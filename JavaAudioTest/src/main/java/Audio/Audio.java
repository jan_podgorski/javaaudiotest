package Audio;

import java.io.Closeable;

public interface Audio extends Closeable, AutoCloseable {
    float getVolume();
    void setVolume(float value);
    float getBalance();
    void setBalance(float value);
    boolean isPlaying();
    boolean isPaused();
    void play();
    void pause();
    void stop();
    void addTrackFinishedListener(TrackFinishedListener l);
}
