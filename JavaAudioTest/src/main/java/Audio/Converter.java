package Audio;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Converter {
    private Converter(){}

    public static void toFile(final byte[] srcBytes, final AudioFormat dstFormat, final String dstPath) throws IOException, UnsupportedAudioFileException {
        if(srcBytes == null || dstFormat == null || dstPath == null){
            throw new IllegalArgumentException("srcBytes, dstFormat nor dstPath cannot be null");
        }

        ByteArrayInputStream bais = null;
        AudioInputStream srcAis = null;
        AudioInputStream mp3Ais = null;
        AudioInputStream pcmAis = null;

        try{
            bais = new ByteArrayInputStream(srcBytes);
            srcAis = AudioSystem.getAudioInputStream(bais);
            AudioFormat srcFormat = srcAis.getFormat();
            AudioFormat mp3tFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, srcFormat.getSampleRate(), 16, srcFormat.getChannels(), srcFormat.getChannels() * 2, srcFormat.getSampleRate(), false);
            mp3Ais = AudioSystem.getAudioInputStream(mp3tFormat, srcAis);
            pcmAis = AudioSystem.getAudioInputStream(dstFormat, mp3Ais);
            AudioSystem.write(pcmAis, AudioFileFormat.Type.WAVE, new File(dstPath));
        } finally {
            closeSilently(bais);
            closeSilently(srcAis);
            closeSilently(mp3Ais);
            closeSilently(pcmAis);
        }
    }

    public static byte[] toBytes(final byte[] srcBytes, final AudioFormat dstFormat) throws IOException, UnsupportedAudioFileException {
        File f = File.createTempFile("temp", "temp");
        toFile(srcBytes, dstFormat, f.getPath());
        byte[] result = Files.readAllBytes(f.toPath());
        f.delete();
        return result;
    }

    private static void closeSilently(Closeable c){
        try{
            c.close();
        }catch(Exception exc){}
    }
}
