package Audio;

class ChunkReader {
    private final byte[] arr;
    private final int chunkSize;
    private int begin = -1;
    private int end = -1;
    public ChunkReader(byte[] arr, int chunkSize){
        if(arr == null){
            throw new IllegalArgumentException("arr cannot be null");
        }

        if(chunkSize <= 0){
            throw new IllegalArgumentException("chunkSize must be > 0");
        }

        this.arr = arr;
        this.chunkSize = chunkSize;

        reset();
    }

    public Range get() {
        return new Range(begin, end);
    }

    public void reset(){
        begin = 0;
        end = chunkSize;
        if(end > arr.length){
            end = arr.length;
        }
    }

    public void nextChunk(){
        begin = end;
        end = (end + chunkSize);
        if(end > arr.length){
            end = arr.length;
        }
    }

    public boolean isFinished(){
        return begin == arr.length;
    }
}
