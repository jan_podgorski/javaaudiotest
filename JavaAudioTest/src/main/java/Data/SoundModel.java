/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.json.JSONObject;

/**
 *
 * @author pi
 */
/*public class SoundModel
{
    public enum FileType
    {
        wav,
        mp3
    }
    private final String kSoundModelNameKey = "name";
    private final String kSoundModelFileTypeKey = "fileType";
    private final String kSoundModelFileSizeKey = "fileSize";
    
    public String name;
    private Path path;
    private Path intermediatePath;
    public FileType fileType;
    public boolean error;
    private long fileSize;
    
    public SoundModel(JSONObject object)
    {
        name = object.optString(kSoundModelNameKey);
        String fileTypeString = object.optString(kSoundModelFileTypeKey);
        fileType = FileType.valueOf(fileTypeString);
        fileSize = object.optLong(kSoundModelFileSizeKey);
        
        System.out.print("xD");
    }
    
    public void checkCachedFiles()
    {
        String expectedSourcePath = prepareSourceFilePath();
        long existingSourceFileSize = FileManager.FileSize(expectedSourcePath);
        if (existingSourceFileSize > 0) {
            Path existingSourcePath = Paths.get(expectedSourcePath);
            setupWithSourceFilePath(existingSourcePath);
        }

        if (fileType == FileType.mp3) {
            String expectedDecodedPath = prepareTargetFilePath();
            long existingDecodedFileSize = FileManager.FileSize(expectedDecodedPath);

            if (existingDecodedFileSize > 0) {
                Path existingDecodedPath = Paths.get(expectedDecodedPath);
                setupWithDecodedFilePath(existingDecodedPath);
            }
        }
    }
    
    public void setupWithSourceFilePath(Path sourceFilePath)
    {
        if (fileType == FileType.wav)
        {
            path = sourceFilePath;
        } else {
            intermediatePath = sourceFilePath;
        }
    }
    
    public void setupWithDecodedFilePath(Path decodedPath)
    {
        path = decodedPath;
    }
    
    public Path getPath()
    {
        return path;
    }
    
    public Path getIntermediatePath()
    {
        return intermediatePath;
    }
    
    public boolean requiresDecoding()
    {
        return fileType == FileType.mp3
                && intermediatePath != null
                && path == null;
    }
    
    public String prepareSourceFilePath()
    {
        String extension = fileType.name();
        return Defines.Defines.targetSoundDirectory + "/" + name + "." + extension;
    }
    
    public String prepareTargetFilePath()
    {
        String extension = FileType.wav.name();
        return Defines.Defines.targetSoundDirectory + "/" + name + "." + extension;
    }
    
    public boolean shouldGetData()
    {
        return (fileType == FileType.wav && path == null )
                || (fileType == FileType.mp3 && intermediatePath == null)
                || error;
    }
    
    
}*/
