/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.File;
import java.util.List;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 *
 * @author pi
 */
/*public class FileManager {
    
    private List<SoundModel> requiredSounds;
    private int currentProcessedFile = 0;
    
    
    
    public FileManager() {}
    
    public void setupWithRequiredFiles(List<SoundModel> sounds)
    {
        requiredSounds = sounds;
    }
    
    public String requiredFileName()
    {
        for(int index = 0; index < requiredSounds.size(); index++)
        {
            SoundModel model = requiredSounds.get(index);
            if (model.shouldGetData())
            {
                currentProcessedFile = index;
                return model.name;
            }
        }
        currentProcessedFile = -1;
        return null;
    }
    
    public void ProcessTempFile(String tempPathString)
    {
        if(currentProcessedFile == -1) { return; }
        
        SoundModel model = requiredSounds.get(currentProcessedFile);
        
        Path sourcePath = Paths.get(tempPathString);
        //Path targetPath = Paths.get(Defines.Defines.targetSoundDirectory, model.name + "." + extension);
        String targetPath = model.prepareSourceFilePath();
        
        File file = new File(targetPath);
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            Files.copy(sourcePath, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            model.setupWithSourceFilePath(file.toPath());
            
            System.out.println("Did write file: " + model.name);
        } catch (Exception ex) {
            model.error = true;
            System.out.println("Error copying temp file");
        }
    }
    
    public static void MoveFile(String sourcePathString, String targetPathString)
    {
        try {
            Path sourcePath = Paths.get(sourcePathString);
            Path targetPath = Paths.get(targetPathString);
            Files.move(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception ex) {
            System.out.println("Error moving decoded file: " + sourcePathString + ", " + targetPathString);
        }
    }
    
    public static long FileSize(String path)
    {
        File file = new File(path);
        if (!file.exists()) { return 0; }
        return file.length();
    }
}*/
