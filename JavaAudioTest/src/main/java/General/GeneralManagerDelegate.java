/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package General;

import java.util.List;

/**
 *
 * @author pi
 */
public interface GeneralManagerDelegate {
    void generalManagerDidReceiveSoundCardList(List<String> soundCArds);
    void generalManagerDidFinishDecodingSounds();
}
