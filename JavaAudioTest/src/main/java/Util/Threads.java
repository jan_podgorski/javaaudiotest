/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pi
 */
public class Threads {
    private static ExecutorService executor;
    private static ScheduledExecutorService scheduledExecutor;
    
    static 
    {
        executor = Executors.newCachedThreadPool();
        scheduledExecutor = Executors.newScheduledThreadPool(128);
    }
    
    public static void async(Runnable task)
    {
        executor.submit(task);
    }
    
    public static void asyncWithDelay(int milliseconds, Runnable task)
    {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.schedule(task, milliseconds, TimeUnit.MILLISECONDS);
        scheduler.shutdown();
    }
}
